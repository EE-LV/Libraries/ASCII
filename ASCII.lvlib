﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_2009_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 2009\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_2009_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 2009\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This library contains VI that return ASCII characters as string and U8.

Copyright (C) 2009  Holger Brand, H.Brand@gsi.de

GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstrasse 1
64291 Darmstadt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Item Name="DC1_CTRL_Q.vi" Type="VI" URL="../DC1_CTRL_Q.vi"/>
		<Item Name="DC2_CTRL_R.vi" Type="VI" URL="../DC2_CTRL_R.vi"/>
		<Item Name="DC3_CTRL_S.vi" Type="VI" URL="../DC3_CTRL_S.vi"/>
		<Item Name="DC4_CTRL_T.vi" Type="VI" URL="../DC4_CTRL_T.vi"/>
		<Item Name="DEL_CTRL_BACKSPACE.vi" Type="VI" URL="../DEL_CTRL_BACKSPACE.vi"/>
		<Item Name="DLE_CTRL_P.vi" Type="VI" URL="../DLE_CTRL_P.vi"/>
		<Item Name="EM_CTRL_Y.vi" Type="VI" URL="../EM_CTRL_Y.vi"/>
		<Item Name="ENQ_CTRL_E.vi" Type="VI" URL="../ENQ_CTRL_E.vi"/>
		<Item Name="EOT_CTRL_D.vi" Type="VI" URL="../EOT_CTRL_D.vi"/>
		<Item Name="ESC_ESCAPE.vi" Type="VI" URL="../ESC_ESCAPE.vi"/>
		<Item Name="ETB_CTRL_W.vi" Type="VI" URL="../ETB_CTRL_W.vi"/>
		<Item Name="ETX_CTRL_C.vi" Type="VI" URL="../ETX_CTRL_C.vi"/>
		<Item Name="FF_CTRL_L.vi" Type="VI" URL="../FF_CTRL_L.vi"/>
		<Item Name="FS_CTRL_BACKSLASH.vi" Type="VI" URL="../FS_CTRL_BACKSLASH.vi"/>
		<Item Name="GS_CTRL_CLOSEBRACKET.vi" Type="VI" URL="../GS_CTRL_CLOSEBRACKET.vi"/>
		<Item Name="HT_TAB.vi" Type="VI" URL="../HT_TAB.vi"/>
		<Item Name="LF_CTRL_J.vi" Type="VI" URL="../LF_CTRL_J.vi"/>
		<Item Name="NAK_CTRL_U.vi" Type="VI" URL="../NAK_CTRL_U.vi"/>
		<Item Name="NULL_CTRL_TWO.vi" Type="VI" URL="../NULL_CTRL_TWO.vi"/>
		<Item Name="RS_CTRL_6.vi" Type="VI" URL="../RS_CTRL_6.vi"/>
		<Item Name="SI_CTRL_O.vi" Type="VI" URL="../SI_CTRL_O.vi"/>
		<Item Name="SO_CTRL_N.vi" Type="VI" URL="../SO_CTRL_N.vi"/>
		<Item Name="SOH_CTRL_A.vi" Type="VI" URL="../SOH_CTRL_A.vi"/>
		<Item Name="space_SPACEBAR.vi" Type="VI" URL="../space_SPACEBAR.vi"/>
		<Item Name="STX_CTRL_B.vi" Type="VI" URL="../STX_CTRL_B.vi"/>
		<Item Name="SUB_CTRL_Z.vi" Type="VI" URL="../SUB_CTRL_Z.vi"/>
		<Item Name="SYNC_CTRL_V.vi" Type="VI" URL="../SYNC_CTRL_V.vi"/>
		<Item Name="US_CTRL_MINUS.vi" Type="VI" URL="../US_CTRL_MINUS.vi"/>
		<Item Name="VT_CTRL_K.vi" Type="VI" URL="../VT_CTRL_K.vi"/>
		<Item Name="ACK_CTRL_F.vi" Type="VI" URL="../ACK_CTRL_F.vi"/>
		<Item Name="BELL_CTRL_G.vi" Type="VI" URL="../BELL_CTRL_G.vi"/>
		<Item Name="BS_BACKSPACE.vi" Type="VI" URL="../BS_BACKSPACE.vi"/>
		<Item Name="CAN_CTRL_X.vi" Type="VI" URL="../CAN_CTRL_X.vi"/>
	</Item>
	<Item Name="ASCII VI Tree.vi" Type="VI" URL="../ASCII VI Tree.vi"/>
</Library>
